reset

set terminal qt font "Helvetica,18"

rhoc= 0.00127999999244924

set xrange [-30:1030]
set yrange [0.963:1.006]

set xlabel 't[M_{sun}]
set ylabel '{/Symbol r}_c/{/Symbol r}_c(0)'

set title "hydrobase-rho.maximum"

set format y "%5.3f"
p 'tov_gallery_polytrop/hydrobase-rho.maximum.asc' u 2:($3/rhoc) w l lw 2 lt 6 notitle
