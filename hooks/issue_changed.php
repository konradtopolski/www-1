<?php header("Content-Type: text/html; charset=utf-8"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <title>Issue webhook received</title>
</head>
<body>
<pre>
<?php

include 'utils.php';

// from https://lornajane.net/posts/2017/handling-incoming-webhooks-in-php
// webhook syntax for bitbucket is here: https://confluence.atlassian.com/bitbucket/event-payloads-740262817.html#EventPayloads-Issueevents
// to test this hook, enable hook history on
// https://bitbucket.org/einsteintoolkit/tickets/admin/addon/admin/bitbucket-webhooks/bb-webhooks-repo-admin
// then use the "Show request body" item to get the payload for a curl call like this:
// curl -H "Content-Type: application/json" -H 'X-Event-Key: issue:comment_created' --request POST --data-binary @- 'https://foo.com/hooks/issue_changed.php?secret=ee472624c1534255ef7b3637d04aea680bf57601d1dd320faef52a75f458c281'
$secret = $_GET['secret'];
// a random number to ensure that not everyone can use the hook
if ($secret != "ee472624c1534255ef7b3637d04aea680bf57601d1dd320faef52a75f458c281") {
  echo ("Invalid hook\n");
  http_response_code(403);
} elseif($json = json_decode(file_get_contents("php://input"), true)) {
  $data = $json;
  $event_key = $_SERVER['HTTP_X_EVENT_KEY'];
  echo ("Event:".$event_key.":\n");

  $subject = "";
  // alternatively, use text/plain and the 'raw' content for the actual
  // markdown and plain ASCII emails
  $html_msg = "<html>";
  $text_msg = "";
  if(isset($data['issue']['milestone']['name']))
    $milestone = $data['issue']['milestone']['name'];
  else
    $milestone = "";
  if(isset($data['issue']['component']['name']))
    $component = $data['issue']['component']['name'];
  else
    $component = "";
  if(isset($data['issue']['version']['name']))
    $version = $data['issue']['version']['name'];
  else
    $version = "";

  $subject = sprintf("#%s: %s", $data['issue']['id'], $data['issue']['title']);
  $html_msg .= sprintf("#%s: %s\n", $data['issue']['id'], $data['issue']['title']);
  $text_msg .= sprintf("#%s: %s\n", $data['issue']['id'], $data['issue']['title']);
  $html_msg .= "<table style='border-spacing: 1ex 0pt; '>\n";
  $html_msg .= sprintf("<tr><td style='text-align:right'>%s:</td><td>%s</td></tr>\n", " Reporter", $data['issue']['reporter']['display_name']);
  $html_msg .= sprintf("<tr><td style='text-align:right'>%s:</td><td>%s</td></tr>\n", "   Status", $data['issue']['state']);
  $html_msg .= sprintf("<tr><td style='text-align:right'>%s:</td><td>%s</td></tr>\n", "Milestone", $milestone);
  $html_msg .= sprintf("<tr><td style='text-align:right'>%s:</td><td>%s</td></tr>\n", "  Version", $version);
  $html_msg .= sprintf("<tr><td style='text-align:right'>%s:</td><td>%s</td></tr>\n", "     Type", $data['issue']['kind']);
  $html_msg .= sprintf("<tr><td style='text-align:right'>%s:</td><td>%s</td></tr>\n", " Priority", $data['issue']['priority']);
  $html_msg .= sprintf("<tr><td style='text-align:right'>%s:</td><td>%s</td></tr>\n", "Component", $component);
  $html_msg .= "</table>\n";
  $html_msg .= "\n";
  $text_msg .= "\n";
  $text_msg .= sprintf("%s: %s\n", " Reporter", $data['issue']['reporter']['display_name']);
  $text_msg .= sprintf("%s: %s\n", "   Status", $data['issue']['state']);
  $text_msg .= sprintf("%s: %s\n", "Milestone", $milestone);
  $text_msg .= sprintf("%s: %s\n", "  Version", $version);
  $text_msg .= sprintf("%s: %s\n", "     Type", $data['issue']['kind']);
  $text_msg .= sprintf("%s: %s\n", " Priority", $data['issue']['priority']);
  $text_msg .= sprintf("%s: %s\n", "Component", $component);
  $text_msg .= "\n";
  switch($event_key) {
  case "issue:updated":
    $html_msg .= sprintf("<p>Changes (by %s):</p>\n", $data['actor']['display_name']);
    $text_msg .= sprintf("Changes (by %s):\n", $data['actor']['display_name']);
    # report all changes other than a change in "content" as a nice table
    $have_changes = false;
      foreach ($data['changes'] as $change => $diff) {
        if($change != "content") {
          if(!$have_changes) {
            $html_msg .= "<p><table>\n";
            $text_msg .= "\n";
            $have_changes = true;
          }
          if ($change == "attachment") {
            # attachements have not "new" and "old" fields
            # href is a list but seems to only ever contain a single entry
            # BUG: right now there seems a bug in the API so that even if multiple
            # files are attached, only one is listed in the json payload
            $html_msg .= sprintf("<tr><td>%s:</td><td><a href=\"%s\">%s</a></td></tr>\n", $change,
                            $diff['links']['self']['href'][0], pr($diff['name']));
            $text_msg .= sprintf("%s: %s (%s)\n", $change,
                            pr($diff['name']), $diff['links']['self']['href'][0]);
          } else {
            $html_msg .= sprintf("<tr><td>%s:</td><td>%s (was %s)</td></tr>\n", $change,
                            pr($diff['new']), pr($diff['old']));
            $text_msg .= sprintf("%s: %s (was %s)\n", $change,
                            pr($diff['new']), pr($diff['old']));
          }
        }
      }
    if($have_changes) {
      $html_msg .= "</table></p>\n";
      $text_msg .= "\n";
    }
    if(isset($data['changes']['content'])) {
      $html_msg .= $data['issue']['content']['html'] . "\n";
      $text_msg .= $data['issue']['content']['raw'] . "\n";
    }
    # a comment block exists all the time even when no comment was made,
    # "raw" is null and "html" is the empty string in that case
    if($data['comment']['content']['html'] != "") {
      $html_msg .= sprintf("<p>Comment (by %s):</p>\n", $data['actor']['display_name']);
      $html_msg .= $data['comment']['content']['html'] . "\n";
      $text_msg .= sprintf("Comment (by %s):\n\n", $data['actor']['display_name']);
      $text_msg .= $data['comment']['content']['raw'] . "\n";
    }
    break;
  case "issue:created":
    $html_msg .= $data['issue']['content']['html'] . "\n";
    $text_msg .= $data['issue']['content']['raw'] . "\n";
    $attachments = call_REST($data['issue']['links']['attachments']['href']);
    if($attachments['size'] > 0) {
      $html_msg .= "<p><table>";
      foreach ($attachments['values'] as $attachment) {
        $html_msg .= sprintf("<tr><td>%s:</td><td><a href=\"%s\">%s</a></td></tr>\n", 'attachment',
                        $attachment['links']['self']['href'][0], pr($attachment['name']));
        $text_msg .= sprintf("%s: %s (%s)\n", 'attachment',
                        pr($attachment['name']), $attachment['links']['self']['href'][0]);
      }
      $html_msg .= "</table></p>\n";
      $text_msg .= "\n";
    }
    break;
  case "issue:comment_created":
    # BUG: attachments do not show up in the comment created json payload
    $html_msg .= sprintf("<p>Comment (by %s):</p>\n", $data['actor']['display_name']);
    $html_msg .= $data['comment']['content']['html'] . "\n";
    $text_msg .= sprintf("Comment (by %s):\n\n", $data['actor']['display_name']);
    $text_msg .= $data['comment']['content']['raw'] . "\n";
    break;
  }
  $html_msg .= "<p>--<br/>\n";
  $text_msg .= "\n--\n";
  $url = $data['issue']['links']['html']['href'];
  $html_msg .= sprintf("Ticket URL: <a href='%s'>%s</a></p>\n", $url, $url);
  $html_msg .= "</html>";
  $text_msg .= sprintf("Ticket URL: %s\n", $url);

  $boundary = "----=_NextPart_" . md5(uniqid(rand()));
  $msg = "This is multipart message using MIME\n";
  $msg .= "--" . $boundary . "\n";
  $msg .= "Content-Type: text/plain; charset=UTF-8\n";
  $msg .= "Content-Transfer-Encoding: quoted-printable". "\n\n";
  $msg .= quoted_printable_encode($text_msg) . "\n";
  $msg .= "--" . $boundary . "\n";
  $msg .= "Content-Type: text/html; charset=UTF-8\n";
  $msg .= "Content-Transfer-Encoding: quoted-printable". "\n\n";
  $msg .= quoted_printable_encode($html_msg) . "\n";
  $msg .= "--" . $boundary . "--\n";

  if ($subject != "") {
    if(isset($data['actor'])) {
      $headers  = sprintf("From: \"%s\" <trac-noreply@einsteintoolkit.org>\r\n", $data['actor']['display_name']);
    } else {
      $headers  = "From: trac-noreply@einsteintoolkit.org\r\n";
    }
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: multipart/alternative; boundary=\"".$boundary."\"\r\n";

    $email = 'trac@einsteintoolkit.org';
    $rc = mail($email, $subject, $msg, $headers);
    echo ("mail sent successfully:".$rc);
  } else {
    echo ("unknown event type, nomail sent");
  }
} else {
  echo ("Invalid request\n");
  http_response_code(400);
}
?>
</pre>

</body>
</html>

